HOST=65.108.145.113

zip -r site.zip index.html assets/

#scp index.html root@${HOST}:/var/www/sbrown.xyz/html/${TARGET_FILE}
#scp deploy-remote.sh sbrown@${HOST}:/var/www/sbrown.xyz/deploy-remote.sh
scp site.zip sbrown@${HOST}:/var/www/sbrown.xyz/html/versus/site.zip

ssh sbrown@${HOST} < deploy-remote.sh

rm site.zip
