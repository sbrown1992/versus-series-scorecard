# Versus Series Scorecard

A scorecard for ranking food, beer, and cocktails in a 4 course format. If the venue does not
provide their own scorecards

## Attribution
* CSS via [Bootstrap](https://getbootstrap.com)
